#!/bin/bash

# VARIABLES
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
echo $SCRIPTPATH

cd $SCRIPTPATH

python3 -m venv venv
. venv/bin/activate

pip install --upgrade pip
pip install --upgrade wheel

pip install -r requirements.txt
