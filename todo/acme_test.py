import logging
import os

from OpenSSL import crypto, SSL
import josepy as jose
import pkg_resources
from acme import client
from acme import messages
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa

logging.basicConfig(level=logging.DEBUG)

DIRECTORY_URL = "https://acme-v01.api.letsencrypt.org/directory"
BITS = 2048  # minimum for Boulder
DOMAIN = "example.com"  # example.com is ignored by Boulder

sans = ("DNS: " + DOMAIN).encode("utf-8")

keyfile = "incommon.key"
csrfile = "incommon.csr"

key = crypto.PKey()
key.generate_key(crypto.TYPE_RSA, BITS)

if os.path.exists(keyfile):
    print("Certificate file exists, aborting." + keyfile)
    exit(1)
else:
    f = open(keyfile, "wb")
    f.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, key))
    f.close()

req = crypto.X509Req()
req.get_subject().CN = "/"
san_constraint = crypto.X509Extension(b"subjectAltName", False, sans)

# Add in extensions
base_constraints = [
    crypto.X509Extension(
        b"keyUsage", False, b"Digital Signature, Non Repudiation, Key Encipherment"
    ),
    crypto.X509Extension(b"basicConstraints", False, b"CA:FALSE"),
]
x509_extensions = base_constraints

x509_extensions.append(san_constraint)

req.add_extensions(x509_extensions)

req.set_pubkey(key)

req.sign(key, "sha256")

if os.path.exists(csrfile):
    print("Certificate file exists, aborting." + csrfile)
    exit(1)
else:
    f = open(csrfile, "wb")
    f.write(crypto.dump_certificate_request(crypto.FILETYPE_PEM, req))
    f.close()

# generate_private_key requires cryptography>=0.5
key = jose.JWKRSA(
    key=rsa.generate_private_key(
        public_exponent=65537, key_size=BITS, backend=default_backend()
    )
)
acme = client.Client(DIRECTORY_URL, key)

regr = acme.register()
logging.info("Auto-accepting TOS: %s", regr.terms_of_service)
acme.agree_to_tos(regr)
logging.debug(regr)

authzr = acme.request_challenges(
    identifier=messages.Identifier(typ=messages.IDENTIFIER_FQDN, value=DOMAIN)
)
logging.debug(authzr)

authzr, authzr_response = acme.poll(authzr)

csr = crypto.dump_certificate_request(crypto.FILETYPE_PEM, req)
try:
    acme.request_issuance(jose.util.ComparableX509(csr), (authzr,))
except messages.Error as error:
    print(
        "This script is doomed to fail as no authorization "
        "challenges are ever solved. Error from server: {0}".format(error)
    )
