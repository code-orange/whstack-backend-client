#!/bin/bash

# VARIABLES
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
echo $SCRIPTPATH

cd $SCRIPTPATH

. venv/bin/activate

./venv/bin/python3 /opt/whstack-backend-client/whstack_client.py >/dev/null 2>&1
