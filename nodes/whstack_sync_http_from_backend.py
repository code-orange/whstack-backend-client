import os


def work(website_id, website):
    web_fhandle = open(
        os.path.join("/etc/nginx/sites-available/site-id-" + str(website_id) + ".conf"),
        "w",
        newline="",
    )
    web_fhandle.write(website["httpfront_nginx"])
    web_fhandle.close()

    paths = (
        "/etc/nginx/tls/certs",
        "/etc/nginx/tls/private",
        "/var/www/site-id-" + str(website_id) + "/log/httpfront",
    )

    for path in paths:
        if not os.path.exists(path):
            os.makedirs(path)

    if "tls_enable" in website["params"]:
        if website["params"]["tls_enable"] == "true":
            tls_cert_fhandle = open(
                os.path.join(
                    "/etc/nginx/tls/certs/site-id-" + str(website_id) + ".crt"
                ),
                "w",
                newline="",
            )
            tls_cert_fhandle.write(website["params"]["tls_cert"])
            tls_cert_fhandle.close()

            tls_key_fhandle = open(
                os.path.join(
                    "/etc/nginx/tls/private/site-id-" + str(website_id) + ".key"
                ),
                "w",
                newline="",
            )
            tls_key_fhandle.write(website["params"]["tls_key"])
            tls_key_fhandle.close()
    else:
        print("Web-ID " + str(website_id) + " has TLS disabled")
