import os
import subprocess

import requests
from decouple import config

BACKEND_URL = config("BACKEND_URL", default="http://localhost", cast=str)
BACKEND_USER = config("BACKEND_USER", default="username", cast=str)
BACKEND_APIKEY = config("BACKEND_APIKEY", default="SECRET", cast=str)

CLUSTER_ID = config("CLUSTER_ID", default=0, cast=int)
NODE_TYPE = config("NODE_TYPE", default="none", cast=str)


def work(website_id, website):
    paths = ("/etc/letsencrypt/config",)

    for path in paths:
        if not os.path.exists(path):
            os.makedirs(path)

    le_conf_file = os.path.join(
        "/etc/letsencrypt/config/site-id-" + str(website_id) + ".conf"
    )

    with open(le_conf_file, "w", newline="") as le_config:
        le_config.write(website["acmecert_certbot_config"])

    certbot_command = list()

    certbot_command.append("/usr/local/bin/certbot")

    certbot_command.append("certonly")
    certbot_command.append("--noninteractive")
    certbot_command.append("--text")
    certbot_command.append("--agree-tos")

    certbot_command.append("--config")
    certbot_command.append(le_conf_file)

    subprocess.call(certbot_command)

    try:
        with open(
            "/etc/letsencrypt/live/site-id-" + str(website_id) + "/fullchain.pem", "rb"
        ) as file_fullchain:
            data_fullchain = file_fullchain.read()

        with open(
            "/etc/letsencrypt/live/site-id-" + str(website_id) + "/privkey.pem", "rb"
        ) as file_privkey:
            data_privkey = file_privkey.read()
    except (FileNotFoundError, IOError):
        return

    push_cert = requests.post(
        BACKEND_URL + "/api/v1/" + NODE_TYPE + "push",
        headers={"Authorization": "ApiKey " + BACKEND_USER + ":" + BACKEND_APIKEY},
        data={
            "website_id": website_id,
            "data_fullchain": data_fullchain,
            "data_privkey": data_privkey,
        },
    )
