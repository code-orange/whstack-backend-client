import os

from decouple import config

BACKEND_URL = config("BACKEND_URL", default="http://localhost", cast=str)
BACKEND_USER = config("BACKEND_USER", default="username", cast=str)
BACKEND_APIKEY = config("BACKEND_APIKEY", default="SECRET", cast=str)

CLUSTER_ID = config("CLUSTER_ID", default=0, cast=int)
NODE_TYPE = config("NODE_TYPE", default="none", cast=str)

WEBSITE_ID = config("WEBSITE_ID", default=0, cast=int)


def work(website_id, website):
    paths = (
        "/etc/ssl/certs",
        "/etc/ssl/private",
    )

    for path in paths:
        if not os.path.exists(path):
            os.makedirs(path)

    with open(os.path.join("/etc/ssl/certs/host.pem"), "w", newline="") as tls_cert:
        tls_cert.write(website["params"]["tls_cert"])

    with open(os.path.join("/etc/ssl/private/host.key"), "w", newline="") as tls_key:
        tls_key.write(website["params"]["tls_key"])
