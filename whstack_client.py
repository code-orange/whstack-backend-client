import requests
from decouple import config

from nodes.whstack_sync_acme_from_backend import work as acmecert_work
from nodes.whstack_sync_host_from_backend import work as host_work
from nodes.whstack_sync_http_from_backend import work as httpfront_work
from nodes.whstack_sync_pbxproxyweb_from_backend import work as pbxproxyweb_work

BACKEND_URL = config("BACKEND_URL", default="http://localhost", cast=str)
BACKEND_USER = config("BACKEND_USER", default="username", cast=str)
BACKEND_APIKEY = config("BACKEND_APIKEY", default="SECRET", cast=str)

CLUSTER_ID = config("CLUSTER_ID", default=0, cast=int)
NODE_TYPE = config("NODE_TYPE", default="none", cast=str)

WEBSITE_ID = config("WEBSITE_ID", default=0, cast=int)

print("Node type: " + NODE_TYPE)

cluster_config = requests.get(
    f"{BACKEND_URL}/api/v1/{NODE_TYPE}/{CLUSTER_ID}?website_id={WEBSITE_ID}",
    headers={"Authorization": "ApiKey " + BACKEND_USER + ":" + BACKEND_APIKEY},
)

if cluster_config.status_code != 200:
    print("Deployment failed:\r\n")
    print(cluster_config.text)
    exit(cluster_config.status_code)

json_data = cluster_config.json()

for website_id, website in json_data.items():
    if NODE_TYPE == "localnode":
        pass
    elif NODE_TYPE == "acmecert":
        acmecert_work(website_id, website)
    elif NODE_TYPE == "httpfront":
        httpfront_work(website_id, website)
    elif NODE_TYPE == "pbxproxyweb":
        pbxproxyweb_work(website_id, website)
    elif NODE_TYPE == "host":
        if int(website_id) == int(WEBSITE_ID):
            host_work(website_id, website)
    elif NODE_TYPE == "phpworker":
        pass
    else:
        if int(website_id) == int(WEBSITE_ID):
            host_work(website_id, website)
